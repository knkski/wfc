use crate::pixel::Pixel;
use wfc::grid::SquareGrid;
use wfc::traits::Grid;

use yew::prelude::*;

#[derive(Debug)]
pub enum Msg {}

#[derive(PartialEq, Clone, Properties)]
pub struct Properties {
    pub pixels: Vec<Vec<Pixel>>,
}

pub struct ImageGrid {
    grid: SquareGrid<Pixel>,
}

impl Component for ImageGrid {
    type Message = ();
    type Properties = Properties;

    fn create(ctx: &Context<Self>) -> Self {
        Self {
            grid: SquareGrid::from(ctx.props().pixels.clone()),
        }
    }

    fn update(&mut self, _: &Context<Self>, _: Self::Message) -> bool {
        true
    }

    fn changed(&mut self, ctx: &Context<Self>, _: &Self::Properties) -> bool {
        if ctx.props().pixels != self.grid.grid {
            self.grid = SquareGrid::from(ctx.props().pixels.clone());
            true
        } else {
            false
        }
    }
    fn view(&self, _ctx: &Context<Self>) -> Html {
        let style = |px: &Pixel, i, j| {
            let props = vec![
                format!("background-color: {}", px.to_hex()),
                format!("grid-row: {}", i + 1),
                format!("grid-column: {}", j + 1),
            ];
            props.join("; ")
        };

        let size = self.grid.size();
        html! {
                <div class="image-grid" style={ format!("aspect-ratio: {} / {};", size.1, size.0) }>
                    { for self.grid
                        .enumerate()
                        .map(|(coord, px)| {
                              html! {
                                  <span class="px" style={style(px, coord.0, coord.1)}></span>
                              }
                        })
                    }
                </div>
        }
    }
}
