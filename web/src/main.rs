#![recursion_limit = "512"]

mod imagegrid;
mod pixel;

use gloo_file::callbacks::FileReader;
use gloo_file::File;
use gloo_timers::callback::Interval;
use imagegrid::ImageGrid;
use pixel::Pixel;
use std::collections::HashSet;
use web_sys::{HtmlInputElement, InputEvent};
use wfc::{coords::RowCol, grid::SquareGrid, traits::ToPixels, Model as Wave};
use yew::prelude::*;

static DEFAULT_INPUT: &[u8] = include_bytes!("../../samples/Red Maze.png");

pub enum Msg {
    Toggle,
    Tick,
    Done,
    Reset,
    HeightChange(u32),
    WidthChange(u32),
    TileSizeChange(u32),
    FileUpload(File),
    Loaded(Vec<u8>),
    Error(String),
}

fn dragoverhandler(e: DragEvent) -> Msg {
    e.prevent_default();
    e.stop_propagation();
    Msg::Tick
}

fn drophandler(e: DragEvent) -> Msg {
    e.prevent_default();
    e.stop_propagation();

    e.data_transfer()
        .and_then(|data| data.files())
        .and_then(|f| f.get(0))
        .map_or_else(
            || Msg::Error("Couldn't read files!".into()),
            |f| Msg::FileUpload(File::from(f)),
        )
}

fn change_handler(e: Event) -> Msg {
    e.target_unchecked_into::<HtmlInputElement>()
        .files()
        .and_then(|f| f.get(0))
        .map_or_else(
            || Msg::Error("Couldn't read any file!".into()),
            |f| Msg::FileUpload(File::from(f)),
        )
}

fn parse_number(e: InputEvent) -> Result<u32, String> {
    e.target_dyn_into::<HtmlInputElement>()
        .ok_or_else(|| "Element not found".into())
        .and_then(|inp| {
            let value = inp.value();

            if value.is_empty() {
                return Err("Please enter a valid numeric value".into());
            }

            let parsed: u32 = value.parse().map_err(|err| format!("{}", err))?;

            if parsed == 0 {
                return Err("Please enter a number greater than zero".into());
            }

            Ok(parsed)
        })
}

pub struct Model {
    wave: Wave<RowCol, Pixel, SquareGrid<HashSet<Vec<Pixel>>>>,
    rendered: Option<Vec<Vec<Pixel>>>,
    input: SquareGrid<Pixel>,
    running: bool,
    done: bool,
    height: u32,
    width: u32,
    tile_size: u32,
    error: Option<String>,
    _interval: Interval,
    reader: Option<FileReader>,
}

impl Model {
    fn get_default_input() -> Vec<Vec<Pixel>> {
        image::load_from_memory(DEFAULT_INPUT)
            .expect("Couldn't load image!")
            .to_rgb8()
            .rows()
            .map(|r| r.map(|p| Pixel(*p)).collect())
            .collect()
    }

    fn reset(&mut self) {
        let (compatibilities, weights) = self.input.parse(self.tile_size);
        self.wave = Wave::<_, _, SquareGrid<_>>::new(
            (self.height, self.width).into(),
            compatibilities,
            weights,
        );
        self.error = None;
        self.rendered = None;
        self.reader = None;
        self.running = false;
        self.done = false;
    }

    fn tick(&mut self, ctx: &Context<Self>) {
        if let Some(wave) = self.wave.next() {
            self.rendered = Some(
                wave.render()
                    .iter()
                    .map(|row| row.iter().map(|p| p.into()).collect())
                    .collect(),
            );
        } else {
            Component::update(self, ctx, Msg::Done);
        }
    }
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();

    fn create(ctx: &Context<Self>) -> Self {
        let callback = ctx.link().callback(|_| Msg::Tick);
        let _interval = Interval::new(1, move || callback.emit(()));

        let input = SquareGrid::from(Self::get_default_input());
        let (compatibilities, weights) = input.parse(2);
        let wave = Wave::<_, _, SquareGrid<_>>::new((20, 20).into(), compatibilities, weights);

        let mut model = Self {
            wave,
            rendered: None,
            input,
            height: 20,
            width: 20,
            tile_size: 2,
            running: false,
            done: false,
            error: None,
            _interval,
            reader: None,
        };

        model.reset();

        model
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        use Msg::*;
        match msg {
            Toggle => self.running = !self.running,
            Tick => {
                if self.running {
                    self.tick(ctx)
                }
            }
            Done => {
                self.running = false;
                self.done = true;
            }
            Reset => self.reset(),
            HeightChange(h) => {
                self.height = h;
                self.reset();
            }
            WidthChange(w) => {
                self.width = w;
                self.reset();
            }
            TileSizeChange(s) => {
                self.tile_size = s;
                self.reset();
            }
            FileUpload(file) => {
                let link = ctx.link().clone();
                let r = gloo_file::callbacks::read_as_bytes(&file, move |res| match res {
                    Ok(bytes) => link.send_message(Msg::Loaded(bytes)),
                    Err(err) => panic!("Error: {}", err),
                });
                self.reader = Some(r);
            }
            Loaded(data) => {
                let img = image::load_from_memory(&data).unwrap();
                let pixels: Vec<Vec<_>> = img
                    .to_rgb8()
                    .rows()
                    .map(|r| r.map(|p| Pixel(*p)).collect())
                    .collect();

                self.input = SquareGrid::from(pixels);
                self.reset();
            }
            Error(msg) => {
                self.error = Some(msg);
            }
        }

        true
    }

    fn changed(&mut self, _: &Context<Self>, _: &Self::Properties) -> bool {
        false
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let rendered = match &self.rendered {
            Some(r) => html! { <ImageGrid pixels={r.clone()} /> },
            None => html! {},
        };

        let reset = match &self.rendered {
            Some(_) => html! { <button
            class="btn btn-primary form-control"
            onclick={ctx.link().callback(|_| Msg::Reset)}>
            { "Reset ⭯" }
               </button> },
            None => html! {},
        };

        let explanation = html! {
            <div>
                <h6>{ "What is this?" }</h6>
                { "This is an implementation of the " }
                <a href="https://github.com/mxgmn/WaveFunctionCollapse">{ "Wave Function Collapse" }</a>
                {" algorithm. It generates new, similar images from the input image above."}
                {" Press 'Start' to see it generate a new image from the input image."}
            </div>
        };

        html! {
            <div id="content" class="container-fluid">
                <div class="row">
                    <div id="control-panel" class="form-group col-sm">
                        <button
                            class="btn btn-primary form-control"
                                disabled={self.done}
                                onclick={ctx.link().callback(|_| Msg::Toggle)}>
                            { if self.running { "Pause ⏸" } else { "Start ⯈"}  }
                            </button>

                        { reset }

                        <div class="error"> { if let Some(msg) = &self.error { msg } else { "" } }</div>

                        // Display current input image
                        <div
                            class="input-image"
                            ondragover={ctx.link().callback(dragoverhandler)}
                            ondrop={ctx.link().callback(drophandler)}>
                          <div class="form-text"><h6>{ "Input image" }</h6></div>
                          <label for="file-upload">
                              <ImageGrid pixels={self.input.grid.clone()} />
                          </label>
                        </div>
                        <label for="file-upload">{ "Click or drag to upload new input image" }</label>
                        <input type="file" id="file-upload" accept="image/*" onchange={ctx.link().callback(change_handler)} />

                       // Controls
                        <label for="height"><h6>{ "Height" }</h6></label>
                        <input
                            type="number"
                            class="form-control"
                            id="height"
                            min="1"
                            max="50"
                            value={self.height.to_string()}
                            oninput={ctx.link().callback(|e| parse_number(e).map_or_else(Msg::Error, Msg::HeightChange))}
                        />
                        <label for="width"><h6>{ "Width" }</h6></label>
                        <input
                            type="number"
                            class="form-control"
                            id="width"
                            min="1"
                            max="50"
                            value={self.width.to_string()}
                            oninput={ctx.link().callback(|e| parse_number(e).map_or_else(Msg::Error, Msg::WidthChange))}
                         />
                        <label for="tile-size"><h6>{ "Tile Size" }</h6></label>
                        <input
                            type="number"
                            class="form-control"
                            id="tile-size"
                            min="1"
                            max="5"
                            value={self.tile_size.to_string()}
                            oninput={ctx.link().callback(|e| parse_number(e).map_or_else(Msg::Error, Msg::TileSizeChange))}
                         />
                        { explanation }
                    </div>
                    <div id="rendered">{ rendered }</div>
                </div>
            </div>
        }
    }
}

fn main() {
    yew::Renderer::<Model>::new().render();
}
