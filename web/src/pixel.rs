use rgb::RGB8;
use wfc::traits::ToRGB;

#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
pub struct Pixel(pub image::Rgb<u8>);

impl Pixel {
    pub fn to_hex(self) -> String {
        format!(
            "#{:02X}{:02X}{:02X}",
            self.0 .0[0], self.0 .0[1], self.0 .0[2]
        )
    }
}

impl ToRGB for Pixel {
    fn render(&self) -> RGB8 {
        RGB8 {
            r: self.0 .0[0],
            g: self.0 .0[1],
            b: self.0 .0[2],
        }
    }
}

impl From<RGB8> for Pixel {
    fn from(p: RGB8) -> Self {
        Self(image::Rgb([p.r, p.g, p.b]))
    }
}

impl From<&RGB8> for Pixel {
    fn from(p: &RGB8) -> Self {
        Self(image::Rgb([p.r, p.g, p.b]))
    }
}
