use std::ops::Add;

use crate::traits::{Coords, Offset};

fn add(u: u32, i: i32) -> Option<u32> {
    if i.is_negative() {
        u.checked_sub(i.wrapping_abs() as u32)
    } else {
        u.checked_add(i as u32)
    }
}

#[derive(Debug, Hash, PartialEq, Eq, Copy, Clone)]
pub struct SquareOffset(pub i32, pub i32);

impl Offset for SquareOffset {
    fn rot90(&self) -> Self {
        Self(self.1, -self.0)
    }

    fn flip(&self) -> Self {
        Self(self.1, self.0)
    }
}

#[derive(Debug, Hash, PartialEq, Eq, Copy, Clone)]
pub struct RowCol(pub u32, pub u32);

impl Coords for RowCol {
    type Offset = SquareOffset;

    fn neighbors() -> Vec<SquareOffset> {
        vec![
            SquareOffset(-1, -1),
            SquareOffset(-1, 0),
            SquareOffset(-1, 1),
            SquareOffset(0, -1),
            SquareOffset(0, 1),
            SquareOffset(1, 1),
            SquareOffset(1, 0),
            SquareOffset(1, -1),
        ]
    }
}

impl Add<SquareOffset> for RowCol {
    type Output = Option<RowCol>;

    fn add(self, rhs: SquareOffset) -> Self::Output {
        match (add(self.0, rhs.0), add(self.1, rhs.1)) {
            (Some(l), Some(r)) => Some(Self(l, r)),
            _ => None,
        }
    }
}

impl From<(u32, u32)> for RowCol {
    fn from((r, c): (u32, u32)) -> RowCol {
        RowCol(r, c)
    }
}

#[derive(Debug, Hash, PartialEq, Eq, Copy, Clone)]
pub struct CubicHexOffset(i32, i32, i32);

#[derive(Debug, Hash, PartialEq, Eq, Copy, Clone)]
pub struct CubicHex(pub u32, pub u32, pub u32);

impl Coords for CubicHex {
    type Offset = CubicHexOffset;

    fn neighbors() -> Vec<CubicHexOffset> {
        unimplemented!()
    }
}

impl Add<CubicHexOffset> for CubicHex {
    type Output = Option<CubicHex>;

    fn add(self, rhs: CubicHexOffset) -> Self::Output {
        match (add(self.0, rhs.0), add(self.1, rhs.1), add(self.2, rhs.2)) {
            (Some(l), Some(c), Some(r)) => Some(Self(l, c, r)),
            _ => None,
        }
    }
}

impl From<(u32, u32, u32)> for CubicHex {
    fn from((q, r, s): (u32, u32, u32)) -> CubicHex {
        CubicHex(q, r, s)
    }
}
