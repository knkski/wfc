//! Wave function collapse implementation

use std::collections::{HashMap, HashSet};
use std::fmt::Debug;
use std::hash::Hash;

use rand::distributions::WeightedIndex;
use rand::prelude::*;
use rand::random;

use crate::traits::{Coords, Grid};

pub mod coords;
pub mod grid;
pub mod traits;

/// A constraint between two tiles
pub type Constraint<C, T> = (C, T);

/// A set of constraints, i.e. a shape
pub type Constraints<C, T> = Vec<Constraint<C, T>>;

pub type Pattern<T> = Vec<T>;

/// A mapping of tile values to constraint shapes
pub type Compatibilities<O, T> = HashMap<(Pattern<T>, O), HashSet<Pattern<T>>>;

/// A global set of tiles and how often they occur
pub type Weights<T> = HashMap<T, u32>;

/// Calculates the Shannon entropy for the given set of possible states
fn entropy<T>(states: &HashSet<Pattern<T>>, weights: &Weights<Pattern<T>>) -> f32
where
    T: Eq + Hash,
{
    let weight: f32 = states.iter().map(|s| weights[s] as f32).sum();
    let weight_log_weight: f32 = states
        .iter()
        .map(|s| (weights[s] as f32) * (weights[s] as f32).ln())
        .sum();

    let noise: f32 = random::<f32>() / 1000f32;
    weight.ln() - (weight_log_weight / weight) - noise
}

/// A Model
pub struct Model<C, T, G>
where
    C: Coords,
    T: Hash + PartialEq + Eq + Clone + Debug,
    G: Grid<C, HashSet<Pattern<T>>>,
{
    coefficients: G,
    compatibilities: Compatibilities<C::Offset, T>,
    weights: Weights<Pattern<T>>,
    queue: Vec<C>,
    collapsed: bool,
}

impl<C, T, G> Model<C, T, G>
where
    C: Coords,
    T: Hash + PartialEq + Eq + Clone + Debug,
    G: Grid<C, HashSet<Pattern<T>>>,
{
    pub fn new(
        size: C,
        compatibilities: Compatibilities<C::Offset, T>,
        weights: Weights<Pattern<T>>,
    ) -> Self {
        let coefficients: HashSet<Pattern<T>> = weights.keys().cloned().collect();
        Self {
            coefficients: G::from_value(size, coefficients),
            compatibilities,
            weights,
            queue: vec![],
            collapsed: false,
        }
    }

    pub fn size(&self) -> C {
        self.coefficients.size()
    }

    /// Selects a random state from the available states for a given coordinate
    fn random_state(&self, coords: &C) -> Pattern<T> {
        let item = self
            .coefficients
            .get(coords)
            .unwrap_or_else(|| panic!("Got bad coordinates {:?}", coords));

        let (weight_choices, weight_probs): (Vec<Pattern<T>>, Vec<u32>) = self
            .weights
            .iter()
            .filter(|(w, _)| item.contains(*w))
            .fold((vec![], vec![]), |mut memo, item| {
                memo.0.push(item.0.clone());
                memo.1.push(*item.1);
                memo
            });

        let dist = WeightedIndex::new(&weight_probs).unwrap();
        let mut rng = thread_rng();

        weight_choices[dist.sample(&mut rng)].clone()
    }

    fn collapse(&mut self) {
        // Calculate the x, y coordinates with the minimum entropy
        let minent = self
            .coefficients
            .enumerate()
            .filter_map(|(coords, item)| {
                if item.len() > 1 {
                    Some((coords, entropy(item, &self.weights)))
                } else {
                    None
                }
            })
            .min_by(|(_, ent1), (_, ent2)| {
                ent1.partial_cmp(ent2)
                    .expect("Floating point error while comparing entropy!")
            });

        // Collapse the selected position that has the minimum entropy,
        // selecting a concrete value, and start the propagation process
        // by adding that position to the propagation queue.
        match minent {
            Some((coords, _)) => {
                let mut collapsed = HashSet::new();
                collapsed.insert(self.random_state(&coords));
                *self.coefficients.get_mut(&coords).unwrap() = collapsed;

                self.queue.push(coords);
            }
            None => self.collapsed = true,
        }
    }

    fn propagate(&mut self) {
        if let Some(top) = self.queue.pop() {
            let top_states = self
                .coefficients
                .get(&top)
                .unwrap_or_else(|| panic!("Bad coordinates put in queue: {:?}", top))
                .clone();

            // For each neighbor, check each of the neighbor's states against all of this node's
            // states. Remove any that aren't possible, and add the neighbor to the queue if any
            // updates were made.
            for offset in C::neighbors() {
                let neighbor_coords = match top + offset {
                    Some(n) => n,
                    None => continue,
                };
                // println!("Checking {:?} -> {:?} ({:?})", top, neighbor_coords, top_states);
                let new = {
                    if let Some(neighbor) = self.coefficients.get(&neighbor_coords) {
                        let updated = neighbor
                            .iter()
                            .cloned()
                            .filter(|neighbor_state| {
                                for top_state in &top_states {
                                    if let Some(f) =
                                        self.compatibilities.get(&(top_state.clone(), offset))
                                    {
                                        if f.contains(neighbor_state) {
                                            return true;
                                        }
                                    }
                                }
                                false
                            })
                            .collect::<HashSet<_>>();

                        if *neighbor != updated {
                            // println!("UPDATING {:?} -> {:?}", neighbor, updated);
                            Some(updated)
                        } else {
                            None
                        }
                    } else {
                        None
                    }
                };

                if let Some(n) = new {
                    if let Some(neighbor) = self.coefficients.get_mut(&neighbor_coords) {
                        *neighbor = n;
                        if self.coefficients.get(&neighbor_coords).is_some() {
                            self.queue.push(neighbor_coords);
                        }
                    }
                }
            }
        }
    }
}

impl<C, T, G> Iterator for Model<C, T, G>
where
    C: Coords,
    T: Hash + PartialEq + Eq + Clone + Debug,
    G: Grid<C, HashSet<Pattern<T>>> + Clone,
{
    type Item = G;

    /// If we've collapsed, we're done. Otherwise, do one iteration of either
    /// collapsing or propagating the collapse, and return the current state.
    fn next(&mut self) -> Option<Self::Item> {
        if self.collapsed {
            return None;
        }

        if self.queue.is_empty() {
            self.collapse();
        } else {
            self.propagate();
        }

        Some(self.coefficients.clone())
    }
}
