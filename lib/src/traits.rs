use rgb::RGB8;
use std::fmt::Debug;
use std::hash::Hash;
use std::ops::Add;

/// Grid location that can be rendered into an RGB pixel
pub trait ToRGB {
    fn render(&self) -> RGB8;
}

/// Grid/tile that can be rendered into RGB pixels
pub trait ToPixels {
    fn render(&self) -> Vec<Vec<RGB8>>;
}

/// Coordinates in some system
pub trait Coords:
    Sized + Hash + Eq + Add<<Self as Coords>::Offset, Output = Option<Self>> + Debug + Clone + Copy
{
    type Offset: Hash + Eq + Copy + Clone + Debug;

    fn neighbors() -> Vec<Self::Offset>;
}

/// Offset coordinates in some system
pub trait Offset: Sized + Hash + Eq + Debug + Clone + Copy {
    #[must_use]
    fn rot90(&self) -> Self;

    #[must_use]
    fn flip(&self) -> Self;
}

/// Grid of some sort
pub trait Grid<C: Coords, T> {
    /// Create a new grid with the given size along each axis
    fn new(size: C) -> Self;

    /// Create a new grid with the given size along each axis, filled with the given value
    fn from_value<V: Clone + Into<T>>(size: C, value: V) -> Self;

    /// Get the size of the grid
    fn size(&self) -> C;

    /// Get a tile at the given coordinates
    fn get(&self, coords: &C) -> Option<&T>;

    /// Get a mutable tile at the given coordinates
    fn get_mut(&mut self, coords: &C) -> Option<&mut T>;

    /// Get a tile at the given coordinates
    fn get_wrapping(&self, coords: &C) -> &T;

    /// Get a tile at the given coordinates
    fn get_mut_wrapping(&mut self, coords: &C) -> &mut T;

    /// Gets multiple tiles representing a shape
    fn get_shape(&self, shape: &[C]) -> Option<Vec<&T>> {
        shape.iter().map(|c| self.get(c)).collect()
    }

    /// Gets multiple tiles representing a shape
    fn get_shape_wrapping(&self, shape: &[C]) -> Vec<&T> {
        shape.iter().map(|c| self.get_wrapping(c)).collect()
    }

    /// Loop over all (coordinate, tile) pairs in an arbitrary order
    fn enumerate<'b>(&'b self) -> Box<dyn Iterator<Item = (C, &T)> + 'b>;
}
