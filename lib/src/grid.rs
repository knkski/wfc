use std::collections::{HashMap, HashSet};
use std::fmt::Debug;
use std::hash::{BuildHasher, Hash};

use crate::coords::{CubicHex, RowCol, SquareOffset};
use crate::traits::{Coords, Grid, ToPixels, ToRGB};
use crate::{Compatibilities, Pattern, Weights};
use rgb::RGB8;

#[derive(Debug)]
pub struct SquareGrid<T> {
    pub grid: Vec<Vec<T>>,
}

pub struct SquareGridIter<'a, T> {
    grid: &'a SquareGrid<T>,
    row: u32,
    col: u32,
}

impl<'a, T> Iterator for SquareGridIter<'a, T> {
    type Item = (RowCol, &'a T);

    fn next(&mut self) -> Option<Self::Item> {
        match self.grid.grid.get(self.row as usize) {
            Some(row) => match row.get(self.col as usize) {
                Some(item) => {
                    let coords = Some((RowCol(self.row, self.col), item));
                    self.col += 1;
                    coords
                }
                None => {
                    self.col = 0;
                    self.row += 1;
                    self.next()
                }
            },
            None => None,
        }
    }
}

impl<T: Eq + Hash + Copy + Debug> SquareGrid<T> {
    pub fn parse(&self, size: u32) -> (Compatibilities<SquareOffset, T>, Weights<Pattern<T>>) {
        let mut compatibilities: HashMap<(Vec<T>, SquareOffset), HashSet<Vec<T>>> = HashMap::new();
        let mut weights: HashMap<Pattern<T>, u32> = HashMap::new();

        // Convert a size of say (2, 2) to a explicit shape
        let size_to_coords = |origin: RowCol| {
            (0..size)
                .flat_map(|i| {
                    (0..size)
                        .map(|j| RowCol(origin.0 + i, origin.1 + j))
                        .collect::<Vec<_>>()
                })
                .collect::<Vec<_>>()
        };

        let neighbors = RowCol::neighbors();

        let rotcw = |grid: &mut Vec<Vec<T>>| {
            let size = grid.len();
            for i in 0..size / 2 {
                for j in i..size - i - 1 {
                    let current = grid[i][j];
                    grid[i][j] = grid[j][size - i - 1];
                    grid[j][size - i - 1] = grid[size - i - 1][size - j - 1];
                    grid[size - i - 1][size - j - 1] = grid[size - j - 1][i];
                    grid[size - j - 1][i] = current;
                }
            }
        };

        let mut _grid = self.grid.clone();

        for _ in 0..4 {
            rotcw(&mut _grid);
            let grid = SquareGrid::from(_grid.clone());
            for row in 0..grid.size().0 {
                for col in 0..grid.size().1 {
                    let current = RowCol(row, col);
                    let pattern = grid
                        .get_shape_wrapping(&size_to_coords(current))
                        .into_iter()
                        .cloned()
                        .collect::<Vec<_>>();

                    *weights.entry(pattern.clone()).or_insert(0u32) += 1;
                }
            }
        }

        let agrees = |tile: &Pattern<T>, other: &Pattern<T>, offset: &SquareOffset| -> bool {
            let SquareOffset(r, c) = *offset;
            for row in 0..size - r.unsigned_abs() {
                for col in 0..size - c.unsigned_abs() {
                    let tile_row = row + (0.max(r) as u32);
                    let tile_col = col + (0.max(c) as u32);
                    let other_row = row + (0.max(-r) as u32);
                    let other_col = col + (0.max(-c) as u32);

                    let t = tile[(tile_row * size + tile_col) as usize];
                    let o = other[(other_row * size + other_col) as usize];

                    if t != o {
                        return false;
                    }
                }
            }
            true
        };

        let keys: Vec<_> = weights.keys().cloned().collect();
        for w1 in &keys {
            for w2 in &keys {
                for n in &neighbors {
                    if agrees(w1, w2, n) {
                        compatibilities
                            .entry((w1.to_vec(), *n))
                            .or_insert_with(HashSet::new)
                            .insert(w2.to_vec());
                    }
                }
            }
        }

        (compatibilities, weights)
    }
}

impl<T> Grid<RowCol, T> for SquareGrid<T> {
    fn new(size: RowCol) -> Self {
        Self {
            grid: (0..size.0)
                .map(|_| Vec::with_capacity(size.1 as usize))
                .collect(),
        }
    }

    fn from_value<V: Clone + Into<T>>(size: RowCol, value: V) -> Self {
        (0..size.0)
            .map(|_| {
                (0..size.1)
                    .map(|_| value.clone().into())
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>()
            .into()
    }

    fn size(&self) -> RowCol {
        RowCol(self.grid.len() as u32, self.grid[0].len() as u32)
    }

    fn get(&self, coords: &RowCol) -> Option<&T> {
        self.grid
            .get(coords.0 as usize)
            .and_then(|row| row.get(coords.1 as usize))
    }

    fn get_mut(&mut self, coords: &RowCol) -> Option<&mut T> {
        self.grid
            .get_mut(coords.0 as usize)
            .and_then(|row| row.get_mut(coords.1 as usize))
    }

    fn get_wrapping(&self, coords: &RowCol) -> &T {
        let len = self.grid.len();
        let row = &self.grid[coords.0 as usize % len];

        &row[coords.1 as usize % row.len()]
    }

    fn get_mut_wrapping(&mut self, coords: &RowCol) -> &mut T {
        let len = self.grid.len();
        let row = &mut self.grid[coords.0 as usize % len];

        let len = row.len();
        &mut row[coords.1 as usize % len]
    }

    fn enumerate<'b>(&'b self) -> Box<dyn Iterator<Item = (RowCol, &T)> + 'b> {
        Box::new(SquareGridIter {
            grid: self,
            row: 0,
            col: 0,
        })
    }
}

impl<T: Clone> Clone for SquareGrid<T> {
    fn clone(&self) -> Self {
        Self {
            grid: self.grid.to_vec(),
        }
    }
}

impl<T> From<Vec<Vec<T>>> for SquareGrid<T> {
    fn from(grid: Vec<Vec<T>>) -> Self {
        Self { grid }
    }
}

impl<T: ToRGB> ToPixels for SquareGrid<T> {
    fn render(&self) -> Vec<Vec<RGB8>> {
        self.grid
            .iter()
            .map(|row| row.iter().map(|i| i.render()).collect::<Vec<_>>())
            .collect::<Vec<_>>()
    }
}

impl<T: ToRGB, S: Clone + BuildHasher> ToPixels for SquareGrid<HashSet<Pattern<T>, S>> {
    fn render(&self) -> Vec<Vec<RGB8>> {
        self.grid
            .iter()
            .map(|row| {
                row.iter()
                    .map(|possibilities| match possibilities.len() {
                        0 => RGB8 {
                            r: 0xFF,
                            g: 0x77,
                            b: 0xFF,
                        },
                        1 => possibilities.iter().next().unwrap()[0].render(),
                        n => {
                            let mut r = 0;
                            let mut g = 0;
                            let mut b = 0;

                            for pos in possibilities {
                                let rendered = pos[0].render();
                                r += rendered.r as u32;
                                g += rendered.g as u32;
                                b += rendered.b as u32;
                            }

                            RGB8 {
                                r: (r / n as u32) as u8,
                                g: (g / n as u32) as u8,
                                b: (b / n as u32) as u8,
                            }
                        }
                    })
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>()
    }
}

#[derive(Clone)]
pub struct HexGrid<T: Clone> {
    _grid: Vec<Vec<T>>,
}

impl<T> Grid<CubicHex, T> for HexGrid<T>
where
    T: Clone,
{
    fn new(size: CubicHex) -> Self {
        assert!(
            size.0 == size.1 && size.1 == size.2,
            "Size must be uniform!"
        );
        Self {
            _grid: (0..size.0)
                .map(|_| Vec::with_capacity(size.1 as usize))
                .collect(),
        }
    }

    fn from_value<V: Clone + Into<T>>(_size: CubicHex, _value: V) -> Self {
        unimplemented!();
    }

    fn size(&self) -> CubicHex {
        unimplemented!()
    }

    fn get(&self, _coords: &CubicHex) -> Option<&T> {
        unimplemented!()
    }

    fn get_mut(&mut self, _coords: &CubicHex) -> Option<&mut T> {
        unimplemented!()
    }

    fn get_wrapping(&self, _coords: &CubicHex) -> &T {
        unimplemented!()
    }

    fn get_mut_wrapping(&mut self, _coords: &CubicHex) -> &mut T {
        unimplemented!()
    }

    fn enumerate<'b>(&'b self) -> Box<dyn Iterator<Item = (CubicHex, &T)> + 'b> {
        unimplemented!()
    }
}

impl<T: Clone> From<Vec<Vec<T>>> for HexGrid<T> {
    fn from(grid: Vec<Vec<T>>) -> Self {
        Self { _grid: grid }
    }
}

pub struct HexGridIter<T: Clone> {
    _grid: HexGrid<T>,
}

impl<T: Clone> Iterator for HexGridIter<T> {
    type Item = (CubicHex, T);

    fn next(&mut self) -> Option<Self::Item> {
        None
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_grid() {
        let _grid = HexGrid::from(vec![vec![1u32, 2], vec![3, 4]]);
    }

    #[test]
    fn test_parse() {
        let grid = SquareGrid::from(vec![vec![0, 0], vec![0, 0]]);

        let (compatibilities, _weights) = grid.parse(2);

        let mut expected = HashMap::new();
        for offset in RowCol::neighbors() {
            expected.insert(
                (vec![0, 0, 0, 0], offset),
                vec![vec![0, 0, 0, 0]].into_iter().collect(),
            );
        }

        assert_eq!(compatibilities, expected);
    }
}
