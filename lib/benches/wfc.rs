use criterion::{criterion_group, criterion_main, Criterion};
use wfc::grid::SquareGrid;
use wfc::Model;

static SAMPLES: [(&str, u32); 3] = [("Red Maze.png", 2), ("Link.png", 3), ("City.png", 3)];

fn bench(c: &mut Criterion) {
    let mut group = c.benchmark_group("parsing");
    group.sample_size(30);
    for (sample, tile_size) in &SAMPLES {
        group.bench_function(*sample, |b| {
            let img_name = format!("../samples/{}", sample);
            let img =
                image::open(&img_name).unwrap_or_else(|_| panic!("Couldn't open {}!", img_name));
            let pixels: Vec<Vec<_>> = img.to_rgb8().rows().map(|r| r.cloned().collect()).collect();

            let grid = SquareGrid::from(pixels);

            b.iter(|| {
                let _ = grid.parse(*tile_size);
            })
        });
    }
    group.finish();

    let mut group = c.benchmark_group("generating");
    group.sample_size(10);
    for (sample, tile_size) in &SAMPLES {
        group.bench_function(*sample, |b| {
            let img_name = format!("../samples/{}", sample);
            let img =
                image::open(&img_name).unwrap_or_else(|_| panic!("Couldn't open {}!", img_name));
            let pixels: Vec<Vec<_>> = img.to_rgb8().rows().map(|r| r.cloned().collect()).collect();

            let grid = SquareGrid::from(pixels);

            b.iter(|| {
                let (compatibilities, weights) = grid.parse(*tile_size);
                let model =
                    Model::<_, _, SquareGrid<_>>::new((5, 5).into(), compatibilities, weights);
                for _ in model {}
            })
        });
    }
    group.finish();
}

criterion_group!(benches, bench);
criterion_main!(benches);
