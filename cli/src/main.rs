use clap::{self, Parser};
use colorful::{Colorful, RGB};
use rgb::RGB8;
use termion::{clear, cursor};
use wfc::{
    grid::SquareGrid,
    traits::{ToPixels, ToRGB},
    Model,
};

#[derive(Parser)]
#[clap(version = "1.0", author = "Kenneth Koski")]
struct Opts {
    #[clap(long = "tile-size", default_value = "2")]
    tile_size: u32,

    #[clap(long = "output-rows", default_value = "10")]
    output_rows: u32,

    #[clap(long = "output-cols", default_value = "10")]
    output_cols: u32,

    #[clap(about = "Image to generate from")]
    image: String,

    #[clap(
        long = "visualize",
        about = "Visualize intermediate states during collapse"
    )]
    visualize: bool,
}

#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
struct Pixel(image::Rgb<u8>);

impl ToRGB for Pixel {
    fn render(&self) -> RGB8 {
        RGB8 {
            r: self.0 .0[0],
            g: self.0 .0[1],
            b: self.0 .0[2],
        }
    }
}

fn main() -> Result<(), String> {
    let opts = Opts::parse();

    let img = image::open(opts.image).unwrap();
    let pixels: Vec<Vec<_>> = img
        .to_rgb8()
        .rows()
        .map(|r| r.map(|p| Pixel(*p)).collect())
        .collect();

    let input_grid = SquareGrid::from(pixels);
    let (compatibilities, weights) = input_grid.parse(opts.tile_size);

    if opts.visualize {
        println!("\n{}{}{}", cursor::Hide, clear::All, cursor::Goto(1, 1));
    }

    let model = Model::<_, _, SquareGrid<_>>::new(
        (opts.output_rows, opts.output_cols).into(),
        compatibilities,
        weights,
    );

    for state in model {
        if opts.visualize {
            let rendered = state
                .render()
                .into_iter()
                .map(|row| {
                    row.into_iter()
                        .map(|RGB8 { r, g, b }| "▓".color(RGB::new(r, g, b)).to_string())
                        .collect::<String>()
                })
                .collect::<Vec<_>>()
                .join("\n");
            println!("{}{}", cursor::Goto(1, 1), rendered);
        }
    }

    if opts.visualize {
        println!("\n{}", cursor::Show);
    }

    Ok(())
}
