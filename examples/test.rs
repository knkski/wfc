use colored::*;
use termion::{clear, cursor};

use wfc::{coords::RowCol, grid::SquareGrid, traits::Ascii, Model};

#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
enum Terrain {
    Land,
    Coast,
    Sea,
}

fn main() {
    use Terrain::{Coast as C, Land as L, Sea as S};

    let input_matrix = vec![
        vec![L, L, L, L],
        vec![L, L, L, L],
        vec![L, C, C, L],
        vec![C, S, S, C],
        vec![S, S, S, S],
        vec![S, S, S, S],
    ];

    let input_grid = SquareGrid::from(input_matrix);
    let (compatibilities, weights) = input_grid.parse(2);

    println!("DEBUG1: {:?}", input_grid);
    println!("DEBUG2: {:?}", compatibilities);
    println!("DEBUG3: {:?}", weights);
}
