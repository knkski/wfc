use colored::*;
use termion::{clear, cursor};

use wfc::{coords::RowCol, grid::HexGrid, traits::Ascii, Model};

#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
enum Terrain {
    Land,
    Coast,
    Sea,
}

impl Ascii for &Terrain {
    fn render(&self) -> String {
        match *self {
            Terrain::Land => "L".green().to_string(),
            Terrain::Coast => "C".yellow().to_string(),
            Terrain::Sea => "S".blue().to_string(),
        }
    }
}

fn main() {
    use Terrain::{Coast as C, Land as L, Sea as S};

    let input_matrix = vec![
        vec![L, L, L, L],
        vec![L, L, L, L],
        vec![L, C, C, L],
        vec![C, S, S, C],
        vec![S, S, S, S],
        vec![S, S, S, S],
    ];

    let shapes = vec![
        vec![RowCol(1, 0)],
        vec![RowCol(-1, 0)],
        vec![RowCol(0, 1)],
        vec![RowCol(0, -1)],
    ];
    let input_grid = HexGrid::from_2d_array(input_matrix);
    let (compatibilities, weights) = input_grid.parse(&shapes);

    println!("\n{}{}{}", cursor::Hide, clear::All, cursor::Goto(1, 1));

    let model = Model::<_, _, HexGrid<_>>::new((10, 50).into(), compatibilities, weights);

    for state in model {
        println!("{}{}", cursor::Goto(1, 1), state.render());
    }

    println!("\n{}", cursor::Show);
}
